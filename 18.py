class Steam_Libary:
    sad_fact = "not available in Russia"
    def __init__(self, name, price):
        self.name = name
        self.price = price
    def discount(self):
        self.price = self.price*0.5
    def __str__(self):
        return f"{self.name} costs {self.price} RUB"

RS1 = Steam_Libary("Red Dead Redemption 2", 2500)
CDpr1 = Steam_Libary("The Witcher 3: WH", 1200)

RS1.discount()

print(RS1)
print(CDpr1)

class Torrent(Steam_Libary):
    def price_conv(self):
        self.price = 0

EA1 = Torrent("NFS:MW", 800)
EA1.price_conv()
print(EA1)            

print(isinstance(RS1, Steam_Libary))



