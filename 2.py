import time
def decorator(function):
    def func_time():       
        f_start = time.time()
        function()
        f_end = time.time()
        print(f_end - f_start)
    return  func_time

@decorator
def func1():
    print("hello world")

func1()



